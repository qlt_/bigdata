#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import requests
import zipfile
import json
import webcolors
import folium
import random
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import ipywidgets as widgets
from tqdm.auto import tqdm
from pathlib import Path
from concurrent.futures import ThreadPoolExecutor
from IPython.display import display
from PIL import Image
from PIL.Image import UnidentifiedImageError
from sklearn.cluster import KMeans
from warnings import simplefilter
from collections import Counter
from matplotlib.ticker import MaxNLocator
from sklearn.neighbors import NearestNeighbors


# In[12]:


user_dir = 'user'
images_dir = 'images'
json_dir = 'images'

# Load df_final from the CSV file
df_result = pd.read_csv('/app/data/df_result.csv')


# ## Step 4: User selection
# In this part a user will choose photos that he like in order to create a profile. 
# This code processes a dataset of images, extracting features like dominant color and orientation, and then recommends images to users based on their preferences. It trains a NearestNeighbors model from scikit-learn on these features, finds the closest images to a user's preferences, and displays the recommended images.

# ### Step 4.1: Create folder
# This code checks if a directory named `user_dir` exists, and creates it if it doesn't.
# 
# The `os.path.exists()` function is used to check if the directory exists. If it doesn't, the `os.mkdir()` function is used to create the directory.

# In[13]:


# Check if the images_dir exists, create it if not.
if not os.path.exists(user_dir):
    os.mkdir(user_dir)


# ### Step 4.2: prepare interface
# This code provides functions to manage user data for an image recommendation system. It allows users to save their preferences for recommended images, delete their data, and view new recommended images. The system is based on CSV files and uses the Pandas library for data processing.

# In[14]:


def create_user_file(user_name):
    user_file = os.path.join(user_dir, f"{user_name}.csv")
    if not os.path.exists(user_file):
        with open(user_file, 'w') as f:
            f.write('photo_id,liked\n')
    return user_file

def load_user_data(user_file):
    global df_user
    df_user = pd.read_csv(user_file)

def save_user_data(df_user):
    user_file = create_user_file(username_text.value)
    df_user.to_csv(user_file, index=False)

def delete_user_file(button):
    user_file = create_user_file(username_text.value)
    if os.path.exists(user_file):
        os.remove(user_file)
    username_container.close()
    delete_button.close()

def display_random_image(user_data):
    photo_ids_result = set(df_result['photo_id'].tolist())
    photo_ids_user = set(df_user['photo_id'].tolist())
    remaining_photo_ids = list(photo_ids_result - photo_ids_user)

    if remaining_photo_ids:
        image_id = random.choice(remaining_photo_ids)
        return image_id, Image.open(os.path.join(images_dir, f"{image_id}.jpg"))
    else:
        print("L'utilisateur a vu toutes les images.")
        return None, None


def update_display(image_id):
    with open(os.path.join(images_dir, f"{image_id}.jpg"), 'rb') as f:
        img = Image.open(f)
        img = img.resize((int(img.width / 5), int(img.height / 5)))
    
    image_output.clear_output()
    with image_output:
        display(img)

def on_username_submit(button):
    global user_data, current_image_id, current_image
    
    user_name = username_text.value
    user_file = os.path.join(user_dir, f"{user_name}.csv")
    
    if os.path.exists(user_file):
        load_user_data(user_file)
        display(delete_button)
        submit_button.close()
    else:
        display(buttons_container)
        display(image_output)
        username_container.close()
        user_file = create_user_file(user_name)
        load_user_data(user_file)
        user_data = set(df_user["photo_id"].tolist())
        current_image_id, current_image = display_random_image(user_data)
        update_display(current_image_id)

def on_like(button):
    global current_image_id, df_user
    image_data = df_result[df_result['photo_id'] == current_image_id].copy()
    image_data['liked'] = True
    df_user = pd.concat([df_user, image_data], ignore_index=True, sort=False)
    current_image_id, _ = display_random_image(user_data)
    update_display(current_image_id)

def on_next(button):
    global current_image_id, df_user
    image_data = df_result[df_result['photo_id'] == current_image_id].copy()
    image_data['liked'] = False
    df_user = pd.concat([df_user, image_data], ignore_index=True, sort=False)
    current_image_id, _ = display_random_image(user_data)
    update_display(current_image_id)

def on_end(button):
    buttons_container.close()
    image_output.close()
    save_user_data(df_user)


# ### Step 4.3: Run interface
# This code display the interface, you can rerun this cell to go back to the interface for another user.

# In[15]:


username_text = widgets.Text(description='Username:')
submit_button = widgets.Button(description='Submit')
delete_button = widgets.Button(description='Delete')
username_container = widgets.HBox([username_text, submit_button])
submit_button.on_click(on_username_submit)
delete_button.on_click(delete_user_file)

like_button = widgets.Button(description='Like')
next_button = widgets.Button(description='Next')
end_button = widgets.Button(description='End')
buttons_container = widgets.HBox([like_button, next_button, end_button])

like_button.on_click(on_like)
next_button.on_click(on_next)
end_button.on_click(on_end)
image_output = widgets.Output()

display(username_container)


# ### Step 4.4: Display result
# This code displays the recommended images for a user based on their preferences. It first loads the user's preferences from the `user_dir` directory, then loads the image features from the `df_features` dataframe. It then trains a NearestNeighbors model from scikit-learn on the image features, finds the closest images to the user's preferences, and displays the recommended images.

# In[16]:


total_photos = len(df_user)
liked_photos = len(df_user[df_user['liked'] == True])

print(f"Number of photos views : {total_photos}")
print(f"Number of liked photos : {liked_photos}")

display(df_user)


# ## Step 5: Recommendation System
# In this section photos will be recommended from a user's profile

# ### Step 5.1: Function
# 
# Here's a brief summary of the `get_user_preferences` and `get_recommended_images` functions:
# 
# - `get_user_preferences`: Takes a Pandas DataFrame `df_user` containing user data and returns a tuple of the user's dominant color, image orientation, size category, and top three keywords based on their liked images.
# - `get_recommended_images`: Takes a Pandas DataFrame `df_result` containing recommendation results and a tuple of user preferences, and returns a filtered DataFrame containing recommended images that match the user's preferences.

# In[17]:


# Function to get the most frequent value in a given column of a DataFrame
def get_most_frequent_value(df, column):
    return df[column].value_counts().idxmax()

def get_user_preferences(df_user):
    # Filter the DataFrame to keep only liked images
    df_liked = df_user[df_user['liked'] == True].copy()
    
    # Initialize user preferences to None
    dominant_color = orientation = size_category = top_keywords = None

    # If there are liked images
    if not df_liked.empty:
        # Get the most frequent values for the specified columns
        dominant_color = get_most_frequent_value(df_liked, 'dominant_color')
        orientation = get_most_frequent_value(df_liked, 'orientation')
        size_category = get_most_frequent_value(df_liked, 'size_category')

        # Get the top 3 most frequent keywords from the liked images
        top_keywords = df_liked[['keyword_principal', 'keyword_secondary', 'keyword_tertiary']].stack().value_counts().nlargest(3).index

        # Print user preferences
        print("User preferences:")
        print(f"Dominant color: {dominant_color}")
        print(f"Orientation: {orientation}")
        print(f"Size category: {size_category}")
        print("Most frequent keywords:")
        for keyword in top_keywords:
            print(f"  - {keyword}")

    # If there are no liked images
    else:
        print("No liked images to determine user preferences.")
    
    return (dominant_color, orientation, size_category, top_keywords)

def get_recommended_images(df_result, user_preferences):
    dominant_color, orientation, size_category, top_keywords = user_preferences
    
    # If the user has preferences
    if all(preference is not None for preference in user_preferences):
        # Filter the DataFrame based on user preferences
        df_recom = df_result[(df_result['dominant_color'] == dominant_color) &
                             (df_result['orientation'] == orientation) &
                             (df_result['size_category'] == size_category) &
                             (df_result['keyword_principal'].isin(top_keywords) |
                              df_result['keyword_secondary'].isin(top_keywords) |
                              df_result['keyword_tertiary'].isin(top_keywords))].copy()
    else:
        # Return an empty DataFrame if there are no user preferences
        df_recom = pd.DataFrame(columns=df_result.columns)
    
    return df_recom


# ### Step 5.2: Get user preferences
# Display a resume of user preferences, thanks to the function `get_user_preferences` and the dataframe `df_user`. This preferences will be used to recommend photos to the user.

# In[18]:


# Get user preferences
user_preferences = get_user_preferences(df_user)


# ### Step 5.3: Get user recommendations
# Execute the recommendation function to obtain a dataframe of recommended images

# In[19]:


# Get the recommended images based on user preferences
df_recom = get_recommended_images(df_result, user_preferences)

# Display the recommended images DataFrame
display(df_recom)


# ### Step 5.4: Display recommendations
# This code displays recommended images randomly using Jupyter widgets. The images are selected from a DataFrame of recommendations and the user can navigate between them using `Next` and `End` buttons. The code uses callback functions to handle the behavior of the buttons.

# In[20]:


# This function displays the next recommended image from the given list of image IDs
def display_next_recommended_image(df_recom, image_ids):
    if image_ids:
        # Get the last image ID from the list
        image_id = image_ids.pop()
        # Open the corresponding image file and resize it
        with open(os.path.join(images_dir, f"{image_id}.jpg"), 'rb') as f:
            img = Image.open(f)
            img = img.resize((int(img.width / 5), int(img.height / 5)))
        # Clear the output area and display the image
        image_output_recom.clear_output()
        with image_output_recom:
            display(img)
        # Return the displayed image ID and the remaining list of image IDs
        return image_id, image_ids
    else:
        # If there are no more recommended images, print a message and return None
        print("No more recommended images.")
        return None, image_ids

# This function is called when the "Next" button is clicked
def on_next_recom(button):
    global current_image_id_recom, image_ids_recom
    # Display the next recommended image
    current_image_id_recom, image_ids_recom = display_next_recommended_image(df_recom, image_ids_recom)
    # If there are no more recommended images, disable the "Next" button
    if not image_ids_recom:
        print("No more recommended images.")
        next_button_recom.disabled = True
        
# This function is called when the "End" button is clicked
def on_end_recom(button):
    # Close the buttons container and image output widgets
    buttons_container_recom.close()
    image_output_recom.close()

# Get the list of recommended image IDs and shuffle them
image_ids_recom = df_recom['photo_id'].tolist()
random.shuffle(image_ids_recom)

# Create "Next" and "End" buttons, and put them in a horizontal box
next_button_recom = widgets.Button(description='Next')
end_button_recom = widgets.Button(description='End')
buttons_container_recom = widgets.HBox([next_button_recom, end_button_recom])

# Attach the "Next" and "End" button callbacks
next_button_recom.on_click(on_next_recom)
end_button_recom.on_click(on_end_recom)

# Create an output area for the displayed images
image_output_recom = widgets.Output()

# Display the first recommended image
current_image_id_recom, image_ids_recom = display_next_recommended_image(df_recom, image_ids_recom)

# Display the buttons and image output widgets
display(buttons_container_recom)
display(image_output_recom)

