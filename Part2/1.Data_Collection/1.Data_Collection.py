#!/usr/bin/env python
# coding: utf-8

# 
# # BigData : massive data processing
# ## Step 0: Prerequisites
# ### 0.1: Install libraries
# 
# This code allows us to install the python libraries needed to run our project.

# In[ ]:


get_ipython().system('pip install wget')
get_ipython().system('pip install requests')
get_ipython().system('pip install Pillow')
get_ipython().system('pip install tqdm')
get_ipython().system('pip install requests')
get_ipython().system('pip install ipywidgets')
get_ipython().system('pip install seaborn')
get_ipython().system('pip install folium')
get_ipython().system('pip install pyspark')


# ### Step 0.2: Import
# This code import libraries needed to run our project.

# In[1]:


import os
import requests
import zipfile
import json
import pandas as pd
from tqdm.auto import tqdm
from pathlib import Path
from concurrent.futures import ThreadPoolExecutor
from IPython.display import display


# ### Step 0.3 : Define variables
# 
# In this step, we define the variables used in the project and usefull to modfify if you want to run the project differently.

# In[2]:


url = "https://unsplash.com/data/lite/latest"
zip_file_path = "unsplash-research-dataset-lite-latest.zip"
output_dir = 'dataset'
user_dir = 'user'
images_dir = 'images'
json_dir = 'images'
ai_dir = 'AI'
limit = 10


# ## Step 1: Data acquisition from unsplash
# ### Step 1.0: Define functions
# In this step, we define the functions used in the step 1

# In[3]:


# Get the 3 keywords with the highest ai_service_1_confidence for the given photo_id
def get_top_3_keywords(photo_id, df):
    photo_keywords = df[df['photo_id'] == photo_id]
    sorted_keywords = photo_keywords.sort_values('ai_service_1_confidence', ascending=False)
    primary_keyword = sorted_keywords.iloc[0]['keyword'] if len(sorted_keywords) > 0 else None
    secondary_keyword = sorted_keywords.iloc[1]['keyword'] if len(sorted_keywords) > 1 else None
    tertiary_keyword = sorted_keywords.iloc[2]['keyword'] if len(sorted_keywords) > 2 else None
    return primary_keyword, secondary_keyword, tertiary_keyword

# Processes a single photo_id by getting its top 3 keywords and returning a dictionary with the photo_id and its keywords.
def process_photo_id(photo_id, df):
    primary, secondary, tertiary = get_top_3_keywords(photo_id, df)
    return {'photo_id': photo_id,
            'keyword_principal': primary,
            'keyword_secondary': secondary,
            'keyword_tertiary': tertiary}
    
# Function to download an image and create a JSON file for it
def download_image_and_create_json(row):
    image_url = row.photo_image_url
    image_id = row.photo_id
    image_path = os.path.join(images_dir, f"{image_id}.jpg")
    json_path = os.path.join(images_dir, f"{image_id}.json")

    # Download the image
    response = requests.get(image_url)
    with open(image_path, 'wb') as image_file:
        image_file.write(response.content)

    # Create a JSON file for the image
    with open(json_path, 'w', encoding='utf-8') as json_file:
        json.dump(row._asdict(), json_file, ensure_ascii=False, indent=4)
        


# ### Step 1.1: Unsplash data set downloading
# 
# In this step we download the dataset from `Unsplash`. We use this dataset because it is **free and free to use**, it offers more than 25 000 images of good quality with tags (not on the 25 000 but a good part) which will allow us to carry out this project well.

# In[4]:


# Get the file size of the zip file
response = requests.get(url, stream=True)
file_size = int(response.headers.get('content-length', 0))

# If the content-length header is not set, try to get the file size by downloading it
if file_size == 0:
    file_size = int(response.headers.get('Content-Length', os.path.getsize(zip_file_path)))

# Download the zip file
response = requests.get(url, stream=True)
with open(zip_file_path, 'wb') as zip_file:
    with tqdm(total=file_size, unit='B', unit_scale=True, unit_divisor=1024, desc="Downloading") as progress_bar:
        for data in response.iter_content(chunk_size=1024):
            zip_file.write(data)
            progress_bar.update(len(data))


# ### Step 1.2: Extract DataSet
# This part allows to extract the content of the dataset previously downloaded in the `output_dir` folder and then to delete the archive in order to keep the directory clean.

# In[5]:


# Check if the outpout_dir exists, create it if not, and clear its contents if it does exist
Path(output_dir).mkdir(exist_ok=True)
for file in os.listdir(output_dir):
    file_path = os.path.join(output_dir, file)
    if os.path.isfile(file_path):
        os.unlink(file_path)

# Extract the zip file
with zipfile.ZipFile(zip_file_path, 'r') as zip_ref:
    zip_ref.extractall(output_dir)

# Delete the zip file
os.remove(zip_file_path)


# ### Step 1.3: Create df_photos with Complete Information
# 
# In this step, we read the `photos.tsv` file into a new DataFrame named `df_photos`. We then remove rows with NaN values in a specifi columns.
# 

# In[6]:


# Read the photos.tsv file
df_photos = pd.read_table(f"{output_dir}/photos.tsv000", delimiter='\t')

# Define a list of columns to check for non-null values (all the population have value in these columns, but you can add more if you want)
required_columns = ['photo_width', 'photo_height', 'photo_location_latitude', 'photo_location_longitude']

# Keep rows that have at least one non-null value in the specified columns
df_photos = df_photos[df_photos[required_columns].notnull().any(axis=1)]

display(df_photos)


# ### Step 1.3.5: Pick random photos
# 
# In this optionnal step, we created the `df_rand` DataFrame to keep only `limit` rows. This is done to reduce the size of the DataFrame and to speed up the execution of the following steps.

# In[7]:


# Get a random sample of size limit, do not execute if you want to use all the population
df_rand = df_photos.sample(n=limit)
display(df_rand)


# ### Step 1.4: Create df_keywords with the Top 3 Keywords
# 
# In this step, we use the `photo_id` values from `df_rand` to create a new DataFrame `df_keywords` with the top 3 keywords for each photo. We use a ThreadPoolExecutor to parallelize the process and a progress bar to show the progress.

# In[8]:


# Read the keywords.tsv file
df_key = pd.read_table(f"{output_dir}/keywords.tsv000", delimiter='\t')

# Get the photo_ids from the df_rand DataFrame
unique_photo_ids = df_rand['photo_id'].unique()

# Create a new DataFrame to store the search results
df_keywords = pd.DataFrame(columns=['photo_id', 'keyword_principal', 'keyword_secondary', 'keyword_tertiary'])

# Use a ThreadPoolExecutor to parallelize the process
with ThreadPoolExecutor() as executor:
    results = list(tqdm(executor.map(lambda x: process_photo_id(x, df_key), unique_photo_ids), total=len(unique_photo_ids), desc="Get top 3 keywords for each photo"))
    
# Add the results to the df_keywords DataFrame
for result in results:
    new_row = pd.DataFrame(result, index=[0])
    df_keywords = pd.concat([df_keywords, new_row], ignore_index=True)

display(df_keywords)


# ### Step 1.5: Merge df_rand and df_keywords into df_final
# 
# In this step, we merge the `df_keywords` DataFrame containing the `photo_id` and keyword information with the `df_rand`
# 

# In[9]:


# Merge the df_keywords and df_rand DataFrames based on the photo_id
df_final = df_keywords.merge(df_rand, on='photo_id', how='left')

display(df_final)


# ### Step 1.6: Download image and get metadata from Unsplash dataset
# In this code block, we will download all images present in the `df_final` DataFrame using the values in the "photo_image_url" column. We will also create a JSON file for each downloaded image containing all photo data from `df_final`. The JSON file will have the same name as the image. All files will be downloaded into the `images_dir` folder. If the folder does not exist, it will be created, and if it exists, its contents will be cleared. A progress bar will be displayed, and the process will be parallelized when possible.

# In[10]:


# Check if the images_dir exists, create it if not, and clear its contents if it does exist
Path(images_dir).mkdir(exist_ok=True)
for file in os.listdir(images_dir):
    file_path = os.path.join(images_dir, file)
    if os.path.isfile(file_path):
        os.unlink(file_path)

# Use a ThreadPoolExecutor to parallelize the process
with ThreadPoolExecutor() as executor:
    list(tqdm(executor.map(download_image_and_create_json, df_final.itertuples(index=False)), total=len(df_final), desc="Downloading images and creating JSON files"))


# In[11]:


# Save df_final to a CSV file
df_final.to_csv('/app/data/df_final.csv', index=False)

# Save images_dir and json_dir to a text file
with open('/app/data/dirs.txt', 'w') as file:
    file.write(f"{images_dir}\n{json_dir}")

