#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import requests
import zipfile
import json
import webcolors
import folium
import random
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import ipywidgets as widgets
from tqdm.auto import tqdm
from pathlib import Path
from concurrent.futures import ThreadPoolExecutor
from IPython.display import display
from PIL import Image
from PIL.Image import UnidentifiedImageError
from sklearn.cluster import KMeans
from warnings import simplefilter
from collections import Counter
from matplotlib.ticker import MaxNLocator
from sklearn.neighbors import NearestNeighbors


# In[2]:


# Load df_final from the CSV file
df_final = pd.read_csv('/app/data/df_final.csv')

# Load images_dir and json_dir from the text file
with open('/app/data/dirs.txt', 'r') as file:
    lines = file.readlines()
    images_dir = lines[0].strip()
    json_dir = lines[1].strip()


# 
# ## Step 2: Additional Tagging
# This step allows to add additional information, not contained in the data provided by the api, to the images.
# 
# ### Step 2.1: DataFrame update
# This code snippet creates a new DataFrame called `df_result` by making a copy of the original DataFrame `df_final`. It then adds three new columns to the `df_result` DataFrame: 'dominant_color', 'orientation', and 'size_category'. These columns are initially set to None for all rows. Finally, the resulting DataFrame with the new columns is displayed.
# 

# In[3]:


# Make a copy of the original DataFrame to store the results
df_result = df_final.copy()

# Add new columns to store the dominant color, orientation, and size category for each image
df_result['dominant_color'] = None
df_result['orientation'] = None
df_result['size_category'] = None

# Display the resulting DataFrame with the new columns
display(df_result)


# ### Step 2.2: Color tagging with K-Means
# In this part you can choose to use two different functions to determine a predominant colour: with the `webcolors` library containing 149 different colours or with a list of 20 colours defined by hand. This does not change the rest of the code, it is simply necessary to take into account that with a small set of data coupled with the use of `webcolors`, it is possible that the precision is too strict and therefore does not allow to group images according to this parameter.
# 
# So the first bloc code defines a dictionary called `CUSTOM_COLORS` containing 20 custom color names and their corresponding RGB values. The `closest_color` function takes an input color in the form of an RGB tuple and finds the closest custom color name based on the Euclidean distance between the RGB components. The function iterates through all the custom color names and their corresponding RGB values, calculates the squared differences for each color component, and stores the sum of the squared differences in a dictionary with the color name as the key. The function then returns the color name with the minimum sum of squared differences.

# In[4]:


# Creation of a dictionary to store the dominant colors for each image
CUSTOM_COLORS = {
    'red': (255, 0, 0),
    'green': (0, 255, 0),
    'blue': (0, 0, 255),
    'yellow': (255, 255, 0),
    'cyan': (0, 255, 255),
    'magenta': (255, 0, 255),
    'white': (255, 255, 255),
    'black': (0, 0, 0),
    'gray': (128, 128, 128),
    'orange': (255, 165, 0),
    'purple': (128, 0, 128),
    'brown': (165, 42, 42),
    'pink': (255, 192, 203),
    'lime': (0, 255, 0),
    'teal': (0, 128, 128),
    'navy': (0, 0, 128),
    'maroon': (128, 0, 0),
    'olive': (128, 128, 0),
    'gold': (255, 215, 0),
    'silver': (192, 192, 192)
}

def closest_color(requested_color):
    # Initialize a dictionary to store the color differences
    min_colors = {}
    
    # Iterate through the custom color names and their corresponding RGB values
    for key, name in CUSTOM_COLORS.items():
        # Extract the RGB components from the color tuple
        r_c, g_c, b_c = name
        
        # Calculate the squared difference for each color component
        rd = (r_c - requested_color[0]) ** 2
        gd = (g_c - requested_color[1]) ** 2
        bd = (b_c - requested_color[2]) ** 2
        
        # Store the sum of the squared differences in the dictionary with the color name as the key
        min_colors[key] = rd + gd + bd
    
    # Return the color name with the minimum sum of squared differences
    return min(min_colors, key=min_colors.get)


# The second `closest_color` function takes an input color in the form of an RGB tuple and finds the closest CSS3 color name based on the Euclidean distance between the RGB components. The function iterates through all the CSS3 color names and their corresponding hex values, converts the hex values to RGB, and calculates the squared differences for each color component. It stores the sum of the squared differences and the color name in a dictionary. The function then returns the color name with the minimum sum of squared differences.

# In[5]:


def closest_color(requested_color):
    # Initialize a dictionary to store the color differences
    min_colors = {}
    
    # Iterate through the CSS3 color names and their corresponding hex values
    for key, name in webcolors.CSS3_HEX_TO_NAMES.items():
        # Convert the hex color to its RGB components
        r_c, g_c, b_c = webcolors.hex_to_rgb(key)
        
        # Calculate the squared difference for each color component
        rd = (r_c - requested_color[0]) ** 2
        gd = (g_c - requested_color[1]) ** 2
        bd = (b_c - requested_color[2]) ** 2
        
        # Store the sum of the squared differences as the key and the color name as the value
        min_colors[(rd + gd + bd)] = name
    
    # Return the color name with the minimum sum of squared differences
    return min_colors[min(min_colors.keys())]


# The `get_dominant_color` function takes an image file path as input and returns the closest custom color name representing the dominant color in the image. The function opens the image file using the PIL Image module and converts it to a NumPy array. The image array is then reshaped into a 2D array with RGB values as rows. The KMeans clustering algorithm from scikit-learn is initialized with one cluster, and it is fit to the reshaped image array. The `closest_color` function, which was defined earlier, is used to find the closest custom color name for the cluster center, representing the dominant color in the image. Finally, the dominant color name is returned.

# In[6]:


def get_dominant_color(image_path, max_size=200):
    # Open the image file using PIL's Image module
    image = Image.open(image_path)
    
    # Reduce the image size to speed up processing
    image.thumbnail((max_size, max_size))
    
    # Convert the image to a NumPy array
    image_array = np.array(image)
    
    # Reshape the image array into a 2D array with RGB values as rows
    image_array = image_array.reshape((image_array.shape[0] * image_array.shape[1], 3))
    
    # Initialize the KMeans clustering algorithm with one cluster
    kmeans = KMeans(n_clusters=1)
    
    # Fit the KMeans algorithm to the reshaped image array
    kmeans.fit(image_array)
    
    # Use the closest_color function to find the closest custom color name for the cluster center
    dominant_color = closest_color(kmeans.cluster_centers_[0])
    
    # Return the dominant color name
    return dominant_color


# ### Step 2.3: Get image orientation
# The `get_orientation` function takes the width and height values of an image as input and returns the orientation of the image. The orientation can be "landscape" if the width is greater than the height, "portrait" if the width is less than the height, or "square" if the width is equal to the height. The function compares the width and height values and returns the appropriate orientation string.

# In[7]:


def get_orientation(width, height):
    # Compare the width and height values of an image
    if width > height:
        # If width is greater than height, return "landscape"
        return "landscape"
    elif width < height:
        # If width is less than height, return "portrait"
        return "portrait"
    else:
        # If width is equal to height, return "square"
        return "square"


# ### Step 2.4: Get image size 
# The `get_size_category` function takes the width and height values of an image as input and returns the size category of the image. The size categories are determined based on the total number of pixels (width * height) in the image. The possible size categories are:
# 
# - "small" for images with 12,250,000 pixels or less (e.g., 3500 x 3500)
# - "medium" for images with 25,000,000 pixels or less (e.g., 5000 x 5000)
# - "large" for images with 49,000,000 pixels or less (e.g., 7000 x 7000)
# - "extra_large" for images with more than 49,000,000 pixels
# 
# The function calculates the total number of pixels and returns the appropriate size category string, you can modify or add categories if you wish.
# 

# In[8]:


def get_size_category(width, height):
    # Calculate the total size (pixels) of the image by multiplying width and height
    size = width * height
    
    # Check the size category based on the total number of pixels
    if size <= 12250000:  # 3500 x 3500
        return "small"
    elif size <= 25000000:  # 5000 x 5000
        return "medium"
    elif size <= 49000000:  # 7000 x 7000
        return "large"
    else:
        return "extra_large"


# ### Step 2.5: Image processing
# 
# In order to reduce the algorithmic complexity a single loop is used to apply our different questions to the images.
# 
# Firstly, The `process_image_and_update_json` function takes an image path, a JSON file path, and an index as input. It performs the following steps:
# 
# 1. It gets the dominant color of the image using the `get_dominant_color` function.
# 2. It reads the JSON file and extracts the width and height values of the image.
# 3. It determines the orientation of the image using the `get_orientation` function.
# 4. It determines the size category of the image using the `get_size_category` function.
# 5. It updates the JSON data with the dominant color, orientation, and size category.
# 6. It writes the updated JSON data back to the JSON file.
# 7. It updates the DataFrame (df_result) with the new information at the specified index.
# 
# The function is used to process an image and update its corresponding JSON file with the calculated information, as well as update the DataFrame with the same information.

# In[9]:


def process_image_and_update_json(image_path, json_path, index):
    try:
        # Get the dominant color of the image
        dominant_color = get_dominant_color(image_path)
        
        # Read the JSON file
        with open(json_path, 'r', encoding='utf-8') as json_file:
            data = json.load(json_file)
        
        # Get the width and height values from the JSON data
        width, height = data["photo_width"], data["photo_height"]
        # Determine the orientation of the image
        orientation = get_orientation(width, height)
        # Determine the size category of the image
        size_category = get_size_category(width, height)
        
        # Update the JSON data with the dominant color, orientation, and size category
        data['dominant_color'] = dominant_color
        data['orientation'] = orientation
        data['size_category'] = size_category
        
        # Write the updated JSON data back to the file
        with open(json_path, 'w', encoding='utf-8') as json_file:
            json.dump(data, json_file, indent=4)
        
        # Update the DataFrame with the new information
        df_result.at[index, 'dominant_color'] = dominant_color
        df_result.at[index, 'orientation'] = orientation
        df_result.at[index, 'size_category'] = size_category

    except UnidentifiedImageError:
        print(f"Erreur avec l'image {image_path}. Suppression du df_result.")
        df_result.drop(index, inplace=True)


# Secondly, the `process_images` function takes a DataFrame as input and performs the following steps:
# 
# 1. It creates lists of image paths, JSON paths, and DataFrame indexes by iterating through the files in the `images_dir` and `json_dir` directories.
# 2. It filters out any future warning messages that might appear during the execution of the code.
# 3. It processes each image and its corresponding JSON file using the `process_image_and_update_json` function. This function updates the JSON files and the `df_result` DataFrame with the dominant colors, orientations, and size categories of the images.
# 4. It uses a for loop to iterate through the images and JSON files. Alternatively, you can uncomment the `ThreadPoolExecutor` lines to use parallelization for a faster execution.
# 
# The code is used to process all the images and their corresponding JSON files in the given directories, updating both the JSON files and the DataFrame with the calculated information.
# 

# In[10]:


image_paths = [os.path.join(images_dir, f) for f in os.listdir(images_dir) if f.endswith('.jpg')]
json_paths = [os.path.join(json_dir, f.replace('.jpg', '.json')) for f in os.listdir(json_dir) if f.endswith('.jpg')]
indexes = [df_result.index[df_result['photo_id'] == os.path.splitext(os.path.basename(image_path))[0]].tolist()[0] for image_path in image_paths]

# You can uncomment the following line to ignore FutureWarning messages that can appear when you run the code
simplefilter("ignore", category=FutureWarning) 

# Use ThreadPoolExecutor for parallel processing
with ThreadPoolExecutor() as executor:
    list(tqdm(executor.map(process_image_and_update_json, image_paths, json_paths, indexes), total=len(image_paths), desc="Updating JSON files and df_result with dominant colors, orientation and size category"))


# In[12]:


display(df_result)


# ### Step 2.6: Display the result
# Finally, the final result stored in `df_result` is displayed 

# In[13]:


# Save df_final to a CSV file
df_result.to_csv('/app/data/df_result.csv', index=False)

