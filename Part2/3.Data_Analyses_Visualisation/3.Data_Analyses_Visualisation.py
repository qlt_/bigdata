#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import requests
import zipfile
import json
import webcolors
import folium
import random
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import ipywidgets as widgets
from tqdm.auto import tqdm
from pathlib import Path
from concurrent.futures import ThreadPoolExecutor
from IPython.display import display
from PIL import Image
from PIL.Image import UnidentifiedImageError
from sklearn.cluster import KMeans
from warnings import simplefilter
from collections import Counter
from matplotlib.ticker import MaxNLocator
from sklearn.neighbors import NearestNeighbors


# In[2]:


# Load df_final from the CSV file
df_result = pd.read_csv('/app/data/df_result.csv')


# ## Step 3: Visualisation
# In this step, the objective is to visualise the different characteristics of all uploaded images.
# ### Step 3.1: Top 10 keywords
# This code snippet visualizes the top 10 most common keywords in the dataset by creating a bar chart. It first concatenates the `keyword_principal`, `keyword_secondary`, and `keyword_tertiary` columns, and then drops any null values. The occurrences of each keyword are counted using the Counter class from the collections module. The total number of images with non-null keywords is printed, along with the total number of images. The 10 most common keywords are then extracted, and their frequencies are plotted using the seaborn library. The x-axis represents the keywords, while the y-axis shows their frequencies.
# 

# In[3]:


# Concatenate the three keyword columns and drop null values
all_keywords = pd.concat([df_result['keyword_principal'], df_result['keyword_secondary'], df_result['keyword_tertiary']]).dropna()

# Count the total number of images with non-null keywords
non_null_keyword_count = all_keywords.count()
images_considered = non_null_keyword_count / 3
total_images = len(df_result)
print(f'Images considered : {images_considered}/{total_images}')

# Count the occurrences of each keyword
keyword_counter = Counter(all_keywords)

# Get the 10 most common keywords
top_keywords = keyword_counter.most_common(10)

# Separate keywords and their counts
keywords, counts = zip(*top_keywords)

# Calculate the percentages
percentages = counts / images_considered * 100

# Plot the bar chart
plt.figure(figsize=(20, 6))
ax = sns.barplot(x=list(keywords), y=list(counts))
plt.title('Top 10 Most Common Keywords')
plt.xlabel('Keyword')
plt.ylabel('Occurence')

# Set y-axis ticks to integers
ax.yaxis.set_major_locator(MaxNLocator(integer=True))

# Display the percentages on the bars
for index, value in enumerate(counts):
    plt.text(x=index, y=value, s=f"{percentages[index]:.2f}%", ha='center', va='bottom')

plt.show()


# Display keyword_counter  
# print(keyword_counter)


# ### Step 3.2: Top 5 Most Common Camera Makes
# 
# In this visualization, we analyze the top 5 most common camera makes based on the `exif_camera_make` column in the dataset. We count the non-null values in the column and display a bar chart showing the 5 most common camera makes and their frequencies.
# 
# Please note that this analysis includes only those images that have non-null values for the `exif_camera_make` column.
# 

# In[4]:


# Count non-null values in 'exif_camera_make' column
non_null_camera_make = df_result['exif_camera_make'].count()
total_images = df_result.shape[0]
print(f"Images considered : {non_null_camera_make}/{total_images}")

# Find the top 5 camera makes
top_camera_makes = df_result['exif_camera_make'].value_counts().head(5)

# Separate makes and counts
makes = top_camera_makes.index
counts = top_camera_makes.values

# Calculate the percentages
percentages = counts / total_images * 100

# Plot the bar chart
plt.figure(figsize=(20, 6))
ax = sns.barplot(x=list(makes), y=list(counts))
plt.title('Top 5 Most Common Camera Makes')
plt.xlabel('Camera Make')
plt.ylabel('Occurence')

# Set y-axis ticks to integers
ax.yaxis.set_major_locator(MaxNLocator(integer=True))

# Display the percentages on the bars
for index, value in enumerate(counts):
    plt.text(x=index, y=value, s=f"{percentages[index]:.2f}%", ha='center', va='bottom')

plt.show()


# ### Step 3.3: Top 10 Most Common Camera Models
# 
# In this visualization, we analyze the top 10 most common camera models based on the `exif_camera_model` column in the dataset. We count the non-null values in the column and display a bar chart showing the 10 most common camera models and their frequencies.
# 
# Please note that this analysis includes only those images that have non-null values for the `exif_camera_model` column.
# 

# In[5]:


# Count non-null values in 'exif_camera_model' column
non_null_camera_model = df_result['exif_camera_model'].count()
total_images = df_result.shape[0]
print(f"Images considered : {non_null_camera_model} / {total_images}")

# Find the top 10 camera models
top_camera_models = df_result['exif_camera_model'].value_counts().head(10)

# Separate models and counts
models = top_camera_models.index
counts = top_camera_models.values

# Calculate the percentages
percentages = counts / total_images * 100

# Plot the bar chart
plt.figure(figsize=(20, 6))
ax = sns.barplot(x=list(models), y=list(counts))
plt.title('Top 10 Most Common Camera Models')
plt.xlabel('Camera Model')
plt.ylabel('Occurence')

# Set y-axis ticks to integers
ax.yaxis.set_major_locator(MaxNLocator(integer=True))

# Display the percentages on the bars
for index, value in enumerate(counts):
    plt.text(x=index, y=value, s=f"{percentages[index]:.2f}%", ha='center', va='bottom')

plt.show()


# ### Step 3.4: Top 10 Most Common Photographer Usernames
# 
# In this visualization, we analyze the top 10 most common photographer usernames based on the `photographer_username` column in the dataset. We count the non-null values in the column and display a bar chart showing the 10 most common photographer usernames and their frequencies.
# 
# Please note that this analysis includes only those images that have non-null values for the `photographer_username` column.

# In[6]:


# Count non-null values in 'photographer_username' column
non_null_photographer_username = df_result['photographer_username'].count()
total_images = df_result.shape[0]
print(f"Images considered : {non_null_photographer_username} / {total_images}")

# Find the top 10 photographer usernames
top_photographer_usernames = df_result['photographer_username'].value_counts().head(10)

# Separate usernames and counts
usernames = top_photographer_usernames.index
counts = top_photographer_usernames.values

# Calculate the percentages
percentages = counts / total_images * 100

# Plot the bar chart
plt.figure(figsize=(20, 6))
ax = sns.barplot(x=list(usernames), y=list(counts))
plt.title('Top 10 Most Common Photographer Usernames')
plt.xlabel('Photographer Username')
plt.ylabel('Occurence')

# Set y-axis ticks to integers
ax.yaxis.set_major_locator(MaxNLocator(integer=True))

# Display the percentages on the bars
for index, value in enumerate(counts):
    plt.text(x=index, y=value, s=f"{percentages[index]:.2f}%", ha='center', va='bottom')

plt.show()


# ### Step 3.5: Top 10 Most Common Dominant Colors
# 
# In this visualization, we analyze the top 10 most common dominant colors based on the `dominant_color` column in the dataset. We count the non-null values in the column and display a bar chart showing the 10 most common dominant colors and their frequencies.
# 
# Please note that this analysis includes only those images that have non-null values for the `dominant_color` column.
# 

# In[7]:


import numpy as np

# Count non-null values in 'dominant_color' column
non_null_dominant_color = df_result['dominant_color'].count()
total_images = df_result.shape[0]
print(f"Images considered : {non_null_dominant_color} / {total_images}")

# Find the top 10 dominant colors
top_dominant_colors = df_result['dominant_color'].value_counts().head(10)

# Separate colors and counts
colors = top_dominant_colors.index
counts = top_dominant_colors.values

# Calculate the percentages
percentages = counts / total_images * 100

# Plot the bar chart
plt.figure(figsize=(20, 6))
ax = sns.barplot(x=list(colors), y=list(counts))
plt.title('Top 10 Most Common Dominant Colors')
plt.xlabel('Dominant Color')
plt.ylabel('Occurence')

# Set y-axis ticks to integers
ax.yaxis.set_major_locator(MaxNLocator(integer=True))

# Display the percentages on the bars
for index, value in enumerate(counts):
    plt.text(x=index, y=value, s=f"{percentages[index]:.2f}%", ha='center', va='bottom')

plt.show()


# ### Step 3.6: Histogram of the photo deposit
# 
# This visualization displays a bar chart representing the number of submitted photos per month-year using the `photo_submitted_at` column. It first counts the non-null values in the `photo_submitted_at` column, then converts the dates to a month-year format, groups the data by month-year, and counts the number of photos for each month-year. The bar chart is plotted using the Seaborn library with the x-axis representing the month-year and the y-axis representing the number of photos.
# 

# In[8]:


# Make a copy of the original dataframe
df_copy = df_result.copy()

# Filter out null or invalid datetime values in 'photo_submitted_at' column
df_copy = df_copy.loc[df_copy['photo_submitted_at'].notnull()]
df_copy = df_copy.loc[df_copy['photo_submitted_at'].str.match('\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}\.\d{3}')]

# Count non-null values in 'photo_submitted_at' column
non_null_submitted = df_copy['photo_submitted_at'].count()
total_photos = len(df_copy)
print(f"Images considered : {non_null_submitted}/{total_photos}")

# Convert 'photo_submitted_at' to datetime format and extract month and year
df_copy['photo_submitted_at'] = pd.to_datetime(df_copy['photo_submitted_at'])
df_copy['month_year'] = df_copy['photo_submitted_at'].dt.to_period('M')

# Group by month-year and count the number of photos
monthly_counts = df_copy.groupby('month_year').size().reset_index(name='count')

# Create a new dataframe to store the histogram results
df_histo = pd.DataFrame({'month_year': monthly_counts['month_year'], 'count': monthly_counts['count']})

# Plot the bar chart
plt.figure(figsize=(20, 6))
ax = sns.barplot(x='month_year', y='count', data=df_histo)
plt.title('Histogram of the photo deposit')
plt.xlabel('Year-Month')
plt.ylabel('Number of Photos')
plt.xticks(rotation=45)

# Set y-axis ticks to integers
ax.yaxis.set_major_locator(MaxNLocator(integer=True))

plt.show()


# ### Step 3.7: Orientation Distribution
# 
# In this visualization, we analyze the distribution of photo orientations based on the `orientation` column in the dataset. We count the non-null values in the column and display a pie chart showing the proportion of landscape, portrait, and square orientations.
# 
# Please note that this analysis includes only those images that have non-null values for the `orientation` column.
# 

# In[9]:


# Count non-null values in 'orientation' column
non_null_orientation = df_result['orientation'].count()
total_images = df_result.shape[0]
print(f"Images considered : {non_null_orientation}/{total_images}")

# Count the number of landscape, portrait, and square orientations
orientation_counts = df_result['orientation'].value_counts()

# Calculate the count for each orientation
orientations_count = df_result['orientation'].value_counts()

# Plot the pie chart
plt.figure(figsize=(6, 6))
plt.pie(orientations_count, labels=orientations_count.index, autopct=lambda pct: f'{int(pct * non_null_orientation / 100):d} ({pct:.1f}%)', startangle=90)
plt.axis('equal')  # Ensures that the pie chart is circular
plt.title('Orientation Distribution')
plt.show()


# ### Step 3.8: Photo Size Category Distribution
# 
# In this visualization, we create a pie chart to show the distribution of the photo size categories in our dataset. The size categories are stored in the `size_category` column, and we will display the number of images and the percentage for each category.
# 
# The code first counts the non-null values in the `size_category` column and calculates the count for each size category. Then, it plots the pie chart with the calculated counts and displays the number of images and the percentage for each category.
# 

# In[10]:


# Count non-null values in 'size_category' column
non_null_size_category = df_result['size_category'].count()
total_images = df_result.shape[0]
print(f"Images considered : {non_null_size_category}/{total_images}")

# Calculate the count for each size category
size_categories_count = df_result['size_category'].value_counts()

# Plot the pie chart
plt.figure(figsize=(6, 6))
plt.pie(size_categories_count, labels=size_categories_count.index, autopct=lambda pct: f'{int(pct * non_null_size_category / 100):d} ({pct:.1f}%)', startangle=90)
plt.axis('equal')
plt.title('Photo Size Category Distribution')
plt.show()


# ### Step 3.9: Photo Size
# 
# In this visualization, we create a scatter plot to show the relationship between `photo_width` and `photo_height` for each image in our dataset. Each point on the graph represents an image, and we will also display special points for the mean, maximum, and minimum values.
# 
# The code first counts the non-null values in the `photo_width` and `photo_height` columns, and then extracts the width and height values for each image. It calculates the mean, maximum, and minimum width and height values and plots a scatter plot with the extracted data. The special points for mean, maximum, and minimum values are displayed with different colors and markers.

# In[11]:


# Count non-null values in 'photo_width' and 'photo_height' columns
non_null_width = df_result['photo_width'].count()
non_null_height = df_result['photo_height'].count()
total_images = df_result.shape[0]
print(f"Images considered : {non_null_width}/{total_images} for width")
print(f"Images considered : {non_null_height}/{total_images} for height")

# Extract photo_width and photo_height
width = df_result['photo_width']
height = df_result['photo_height']

# Calculate mean, max, and min
mean_width = width.mean()
mean_height = height.mean()
max_width = width.max()
max_height = height.max()
min_width = width.min()
min_height = height.min()

# Plot the scatter plot
plt.figure(figsize=(12, 8))
plt.scatter(width, height, label='Images', alpha=0.5)
plt.scatter(mean_width, mean_height, c='red', marker='o', s=100, label='Mean')
plt.scatter(max_width, max_height, c='green', marker='o', s=100, label='Max')
plt.scatter(min_width, min_height, c='blue', marker='o', s=100, label='Min')

plt.xlabel('Photo Width')
plt.ylabel('Photo Height')
plt.title('Photo Size')
plt.legend()
plt.show()


# ### Step 3.10: World Map with Image Locations and Popups
# 
# In this visualization, we create an interactive world map using the `folium` library to display the locations where the images in our dataset were taken. The map uses the `photo_location_latitude` and `photo_location_longitude` columns to plot small blue circles for each image location. When a user clicks on a circle, a popup will appear displaying the `photo_id`. The map is interactive, allowing users to zoom in and out to see the names of countries and cities.
# 
# The code first counts the non-null values in the `photo_location_latitude` and `photo_location_longitude` columns, and then creates a world map using the `folium.Map` function. It then iterates through the rows of the dataframe, adding a circle marker for each image location and a popup containing the `photo_id`. Finally, the map is displayed.
# 

# In[12]:


# Count non-null values in 'photo_location_latitude' and 'photo_location_longitude' columns
non_null_latitude = df_result['photo_location_latitude'].count()
non_null_longitude = df_result['photo_location_longitude'].count()
total_images = df_result.shape[0]
print(f"Images considered : {non_null_latitude}/{total_images} for latitude")
print(f"Images considered : {non_null_longitude}/{total_images} for longitude")

# Create the world map
world_map = folium.Map()

# Add a tile layer with the no_wrap option
folium.TileLayer('cartodb positron', no_wrap=False).add_to(world_map)

# Add markers for each image location with a popup containing the photo_id and ai_description
for index, row in df_result.iterrows():
    lat, lon, photo_id, description = row['photo_location_latitude'], row['photo_location_longitude'], row['photo_id'], row['ai_description']
    if not (pd.isna(lat) or pd.isna(lon)):
        popup_text = f"Photo ID: {photo_id}<br>Description: {description}"
        folium.CircleMarker(
            [lat, lon], radius=2, color='blue', fill=True, fill_color='blue', fill_opacity=1,
            popup=folium.Popup(popup_text, max_width=200)
        ).add_to(world_map)

# Display the map
world_map


# In[ ]:




